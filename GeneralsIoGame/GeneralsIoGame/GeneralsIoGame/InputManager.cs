﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace GeneralsIoGame
{    
    public static class InputManager
    {
        private static MouseState _currentMouseState;
        private static MouseState _lastMouseState;
        private static KeyboardState _currentKeyboardState;
        private static KeyboardState _lastKeyboardState;

        public static MouseState CurrentMouseState
        {
            get { return _currentMouseState; }
        }
        public static MouseState LastMouseState
        {
            get { return _lastMouseState; }
        }
        public static KeyboardState CurrentKeyboardState
        {
            get { return _currentKeyboardState; }
        }
        public static KeyboardState LastKeyboardState
        {
            get { return _lastKeyboardState; }
        }
        public static Point MousePoint
        {
            get { return new Point(CurrentMouseState.X, CurrentMouseState.Y); }
        }

        public static float MouseWheel
        {
            get { return Math.Sign(_currentMouseState.ScrollWheelValue - _lastMouseState.ScrollWheelValue); }
        }

        public static void Update()
        {
            _lastMouseState = _currentMouseState;
            _currentMouseState = Mouse.GetState();

            _lastKeyboardState = _currentKeyboardState;
            _currentKeyboardState = Keyboard.GetState();
        }
    }

}
