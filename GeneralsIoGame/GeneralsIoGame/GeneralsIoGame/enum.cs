﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GeneralsIoGame
{
    public enum ScreenState
    {
        MainMenu,
        Game
    }

    public enum GameMode
    {
        SinglePlayer,
        MultiPlayer
    }
}
