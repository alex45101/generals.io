﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GeneralsIoGame
{
    public class BaseScreen : IScreen
    {
        public string Name { get; private set; }
        public Color BackColor { get; set; }

        public BaseScreen(string name, Color backColor, ContentManager content, GraphicsDevice graphicsDevice)
        {
            Name = name;
            BackColor = backColor;
        }

        public virtual void Update(GameTime gameTime) { }

        public virtual void Draw(SpriteBatch spriteBatch) { }
    }
}
