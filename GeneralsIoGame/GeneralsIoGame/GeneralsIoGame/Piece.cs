﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GeneralsIoGame
{
    public class Piece : TextSprite
    {
        public int Size { get; set; }

        public Piece(SpriteFont font, Vector2 position, Color tint)
            : base(font, "", position, tint)
        {
            Size = 1;
        }
    }
}
