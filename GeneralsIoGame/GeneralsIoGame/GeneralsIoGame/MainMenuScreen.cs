﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;

namespace GeneralsIoGame
{
    public class MainMenuScreen : BaseScreen
    {
        TextSprite title;        
        Button siinglePlayerButton;

        Texture2D buttonImage;

        public MainMenuScreen(ContentManager content, GraphicsDevice graphicsDevice)
            : base("MainMenu", Color.Gray, content, graphicsDevice)
        {
            buttonImage = new Texture2D(graphicsDevice, 300, 50);

            Color[] pixels = new Color[buttonImage.Width * buttonImage.Height];

            int counter = 0;
            for (int i = 0; i < buttonImage.Width; i++)
            {
                for (int j = 0; j < buttonImage.Height; j++)
                {
                    pixels[counter] = Color.Wheat;
                    counter++;
                }
            }

            buttonImage.SetData<Color>(pixels);

            title = new TextSprite(content.Load<SpriteFont>("TitleFont"), "Generals.io Swah Edition", Vector2.Zero, Color.Black);
            title.Origin = new Vector2(title.Font.MeasureString(title.Text).X / 2, title.Font.MeasureString(title.Text).Y / 2);
            title.Position = new Vector2(graphicsDevice.Viewport.Width / 2, title.Font.MeasureString(title.Text).Y);
            
            siinglePlayerButton = new Button(buttonImage, new Vector2(graphicsDevice.Viewport.Width / 2 - buttonImage.Width / 2, graphicsDevice.Viewport.Height / 2 - buttonImage.Height / 2), Color.White, "SinglePlayer", content.Load<SpriteFont>("TitleFont"));
            siinglePlayerButton.TextClickColor = Color.DarkGray;
            siinglePlayerButton.Click += PlayButton_Click;
        }

        private void PlayButton_Click(object sender, EventArgs e)
        {
            ScreenManager.SwicthScreen(ScreenState.Game);
        }

        public override void Update(GameTime gameTime)
        {
            siinglePlayerButton.Update(gameTime);
            base.Update(gameTime);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            title.Draw(spriteBatch);            
            siinglePlayerButton.Draw(spriteBatch);
            base.Draw(spriteBatch);
        }
    }
}
