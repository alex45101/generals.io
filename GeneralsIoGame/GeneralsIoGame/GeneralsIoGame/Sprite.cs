﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GeneralsIoGame
{
    public class Sprite : ISprite
    {
        public Texture2D Image { get; set; }
        public Vector2 Position { get; set; }
        public Color Tint { get; set; }
        public Rectangle SourceRectangle { get; set; }
        public float Rotation { get; set; }
        public Vector2 Origin { get; set; }
        public Vector2 Scale { get; set; }
        public SpriteEffects SpriteEffects { get; set; }
        public float LayerDepth { get; set; }
        public Rectangle Hitbox { get; private set; }
        
        public Sprite(Texture2D image, Vector2 position, Color tint)
               : this(image, position, tint, new Rectangle(0, 0, image.Width, image.Height), 0, Vector2.Zero, Vector2.One, SpriteEffects.None, 0)
        {

        }

        public Sprite(Texture2D image, Vector2 position, Color tint, Rectangle sourceRectangle)
               : this(image, position, tint, sourceRectangle, 0, Vector2.Zero, Vector2.One, SpriteEffects.None, 0)
        {

        }

        public Sprite(Texture2D image, Vector2 position, Color tint, Rectangle sourceRectangle, float rotation)
               : this(image, position, tint, sourceRectangle, rotation, Vector2.Zero, Vector2.One, SpriteEffects.None, 0)
        {

        }

        public Sprite(Texture2D image, Vector2 position, Color tint, Rectangle sourceRectangle, float rotation, Vector2 origin)
               : this(image, position, tint, sourceRectangle, rotation, origin, Vector2.One, SpriteEffects.None, 0)
        {

        }

        public Sprite(Texture2D image, Vector2 position, Color tint, Rectangle sourceRectangle, float rotation, Vector2 origin, Vector2 scale)
            : this(image, position, tint, sourceRectangle, rotation, origin, scale, SpriteEffects.None, 0)
        {

        }

        public Sprite(Texture2D image, Vector2 position, Color tint, Rectangle sourceRectangle, float rotation, Vector2 origin, Vector2 scale, SpriteEffects spriteEffects)
            : this(image, position, tint, sourceRectangle, rotation, origin, scale, spriteEffects, 0)
        {

        }

        public Sprite(Texture2D image, Vector2 position, Color tint, Rectangle sourceRectangle, float rotation, Vector2 origin, Vector2 scale, SpriteEffects spriteEffects, float layerDepth)
        {
            Image = image;
            Position = position;
            SourceRectangle = sourceRectangle;
            Tint = tint;
            Rotation = rotation;
            Origin = origin;
            Scale = scale;
            SpriteEffects = spriteEffects;
            LayerDepth = layerDepth;
            Hitbox = new Rectangle((int)position.X, (int)position.Y, sourceRectangle.Width, sourceRectangle.Height);            
        }

        public virtual void Update(GameTime gameTime)
        {

        }

        public virtual void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(Image, Position, SourceRectangle, Tint, Rotation, Origin, Scale, SpriteEffects, LayerDepth);
        }


        public void DrawHitbox(SpriteBatch spriteBatch, Color color)
        {

        }
    }
}
