﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GeneralsIoGame
{
    public class TextSprite : ISprite
    {
        public SpriteFont Font { get; set; }
        public string Text { get; set; }
        public Vector2 Position { get; set; }
        public Color Tint { get; set; }
        public float Rotation { get; set; }
        public Vector2 Origin { get; set; }
        public Vector2 Scale { get; set; }
        public SpriteEffects SpriteEffects { get; set; }
        public float LayerDepth { get; set; }
        public Rectangle Hitbox { get; private set; }
        
        public TextSprite(SpriteFont font, string text, Vector2 position, Color tint)
            : this(font, text, position, tint, 0, Vector2.Zero, Vector2.One, SpriteEffects.None, 0)
        {

        }

        public TextSprite(SpriteFont font, string text, Vector2 position, Color tint, float rotation)
            : this(font, text, position, tint, rotation, Vector2.Zero, Vector2.One, SpriteEffects.None, 0)
        {

        }

        public TextSprite(SpriteFont font, string text, Vector2 position, Color tint, float rotation, Vector2 origin)
            : this(font, text, position, tint, rotation, origin, Vector2.One, SpriteEffects.None, 0)
        {

        }

        public TextSprite(SpriteFont font, string text, Vector2 position, Color tint, float rotation, Vector2 origin, Vector2 scale)
            : this(font, text, position, tint, rotation, origin, scale, SpriteEffects.None, 0)
        {

        }

        public TextSprite(SpriteFont font, string text, Vector2 position, Color tint, float rotation, Vector2 origin, Vector2 scale, SpriteEffects spriteEffects)
            : this(font, text, position, tint, rotation, origin, scale, spriteEffects, 0)
        {

        }

        public TextSprite(SpriteFont font, string text, Vector2 position, Color tint, float rotation, Vector2 origin, Vector2 scale, SpriteEffects spriteEffects, float layerDepth)
        {
            Font = font;
            Text = text;
            Position = position;
            Tint = tint;
            Rotation = rotation;
            Origin = origin;
            Scale = scale;
            SpriteEffects = spriteEffects;
            LayerDepth = layerDepth;
            Hitbox = new Rectangle((int)position.X, (int)position.Y, (int)font.MeasureString(text).X, (int)font.MeasureString(text).Y);
        }

        public void Update(GameTime gameTime)
        {

        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.DrawString(Font, Text, Position, Tint, Rotation, Origin, Scale, SpriteEffects, LayerDepth);
        }

        public void DrawHitbox(SpriteBatch spriteBatch, Color color)
        {

        }
    }
}
