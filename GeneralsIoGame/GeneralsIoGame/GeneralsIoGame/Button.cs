﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace GeneralsIoGame
{
    public class Button : Sprite
    {
        public event EventHandler Click;
        public event EventHandler<MouseEventArgs> MouseClick;

        private TextSprite _textSprite;                
        
        public string Text { get; set; }        
        public SpriteFont Font { get; set; }        
        public Color TextClickColor { get; set; }        

        public Button(Texture2D texture, Vector2 position, Color tint, string text, SpriteFont font)
            : base(texture, position, tint)
        {
            Text = text;
            Font = font;

            _textSprite = new TextSprite(Font, Text, new Vector2(position.X + Image.Width / 2 - font.MeasureString(text).X / 2, position.Y), tint);
            TextClickColor = Color.Yellow;
        }

        public override void Update(GameTime gameTime)
        {            
            if (Hitbox.Contains(InputManager.CurrentMouseState.X, InputManager.CurrentMouseState.Y))
            {
                _textSprite.Tint = TextClickColor;
            }
            else
            {
                _textSprite.Tint = Tint;
            }

            if (Hitbox.Contains(InputManager.CurrentMouseState.X, InputManager.CurrentMouseState.Y) && InputManager.CurrentMouseState.LeftButton == ButtonState.Released && InputManager.LastMouseState.LeftButton == ButtonState.Pressed)
            {
                //when we trigger the event
                var localClick = Click; //creating local copy so that no events gets unsubscribed.
                if (localClick != null) //is anyone listen?
                {
                    localClick(this, EventArgs.Empty); //trigger the event
                }

                var localclick = MouseClick;
                if (localclick != null)
                {
                    MouseClick(this, new MouseEventArgs(InputManager.CurrentMouseState));
                }
            }

            base.Update(gameTime);
        }

        private void DrawText(SpriteBatch sb)
        {
            _textSprite.Draw(sb);
        }

        public override void Draw(SpriteBatch sb)
        {
            base.Draw(sb);
            DrawText(sb);
        }
    }
}
