﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GeneralsIoGame
{
    public class ScreenManager
    {
        public static ScreenState CurrentScreenState
        {
            get { return screenStack.Peek(); }           
        }

        public static Dictionary<ScreenState, BaseScreen> Screens { get; private set; }

        static Stack<ScreenState> screenStack;

        public ScreenManager()
        {
            Screens = new Dictionary<ScreenState, BaseScreen>();
            screenStack = new Stack<ScreenState>();
        }

        public void AddScreen(BaseScreen screen, ScreenState screenState)
        {
            Screens.Add(screenState, screen);                    
        }

        public static void SwicthScreen(ScreenState screenState)
        {
            screenStack.Push(screenState);
        }

        public static void Back()
        {
            screenStack.Pop();
        }

        public void Update(GameTime gameTime)
        {
            Screens[CurrentScreenState].Update(gameTime);
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            Screens[CurrentScreenState].Draw(spriteBatch);
        }
    }
}
