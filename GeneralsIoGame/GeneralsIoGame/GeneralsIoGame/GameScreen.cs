﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace GeneralsIoGame
{
    public class GameScreen : BaseScreen
    {
        Grid grid;

        public GameScreen(ContentManager content, GraphicsDevice graphicsDevice)
            : base("GameScreen", Color.Gray, content, graphicsDevice)
        {
            grid = new Grid(content.Load<Texture2D>("cellImage"), 13, 7, 2);
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            grid.Draw(spriteBatch);
            base.Draw(spriteBatch);
        }
    }
}
