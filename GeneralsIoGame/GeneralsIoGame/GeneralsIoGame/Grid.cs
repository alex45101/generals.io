﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GeneralsIoGame
{
    public class Grid
    {
        public Texture2D CellImage { get; private set; }
        public int Width { get; private set; }
        public int Height { get; private set; }
        public int Spacing { get; private set; }

        public Grid(Texture2D cellImage, int size, int spacing)
            : this(cellImage, size, size, spacing)
        {

        }

        public Grid(Texture2D cellImage, int width, int height, int spacing)
        {
            CellImage = cellImage;
            Width = width;
            Height = height;
            Spacing = spacing;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            int x = 0;
            int y = 0;
            for (int i = 0; i < Height; i++)
            {
                for (int a = 0; a < Width; a++)
                {
                    spriteBatch.Draw(CellImage, new Vector2(x, y), Color.White);
                    x += CellImage.Width + Spacing;
                }
                y += CellImage.Height + Spacing;
                x = 0;
            }         
        }
    }
}
