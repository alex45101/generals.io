﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GeneralsIoGame
{
    public interface IScreen
    {
        void Update(GameTime gametime);
        void Draw(SpriteBatch spriteBatch);        
    }
}
